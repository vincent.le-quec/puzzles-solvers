const express = require('express');
const router = express.Router();

const Model1 = require('../models/model1');
const Model2 = require('../models/model2');
const Model3 = require('../models/model3');

/* POST test proposition for problem 1 */
router.post('/pb1', function(req, res) {
  console.log(req.body);

  // retrieve my data
  let model = new Model1(req.body);

  // create data file
  let result = model.solve();
  console.log("response :");
  console.log(result.toJson());

  res.send(result.toJson());
});

/* POST test proposition for problem 1 */
router.post('/pb2', async function(req, res) {
  console.log(req.body);

  // retrieve my data
  let model = new Model2(req.body);

  // create data file
  let result = model.solve();
  console.log("response :");
  console.log(result.toJson());

  res.send(result.toJson());
});

/* POST test proposition for problem 1 */
router.post('/pb3', function(req, res) {
  console.log(req.body);

  // retrieve my data
  let model = new Model3(req.body);

  // create data file
  let result = model.solve();
  console.log("response :");
  console.log(result.toJson());

  res.send(result.toJson());
});

module.exports = router;
