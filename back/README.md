# Puzzles Solvers

## Requirements
Nodejs >= v12
Minizinc >= 2.5.3

## Execution
Install project dependencies :
```shel
make install
```

Run server in development mode :
```shell
make dev
```

Run server in production mode :
```shell
make prod
```