const AbstractModel = require('./abstract_model.js');

let shirtsValue = ["blue", "green", "red", "white", "yellow"];
let namesValue = ["Andrea", "Holly", "Julie", "Leslie", "Victoria"];
let surnamesValue = ["Brown", "Davis", "Lopes", "Miller", "Wilson"];
let pastasValue = ["Farfalle", "Lasagne", "Penne", "Spaghetti", "Ravioli"];
let winesValue = ["Australian", "Argentine", "Chilean", "French", "Italian"];
let agesValue = ["30 years","35 years","40 years","45 years","50 years"];

class Model1 extends AbstractModel {
    constructor(women) {
        super();
        this._women = women;
    }

    getProblemPath() {
        return "pb_1";
    }

    toString() {
        return `Model1[n_queen='${this._n_queen}']`;
    }

    toDzn() {
        let shirts = ["_", "_", "_", "_", "_"];
        let names = ["_", "_", "_", "_", "_"];
        let surnames = ["_", "_", "_", "_", "_"];
        let pastas = ["_", "_", "_", "_", "_"];
        let wines = ["_", "_", "_", "_", "_"];
        let ages = ["_", "_", "_", "_", "_"];

        let i = 1;
        for (let key in this._women) {
            // verify we do not have multiple times the same value taken
            if(shirts[shirtsValue.indexOf(this._women[key].Shirt)] != "_" || names[namesValue.indexOf(this._women[key].Name)] != "_" || surnames[surnamesValue.indexOf(this._women[key].Surname)] != "_" || pastas[pastasValue.indexOf(this._women[key].Pasta)] != "_" || wines[winesValue.indexOf(this._women[key].Wine)] != "_" || ages[agesValue.indexOf(this._women[key].Age)] != "_") {
                throw "Incorrect model !";
            }

            shirts[shirtsValue.indexOf(this._women[key].Shirt)] = i;
            names[namesValue.indexOf(this._women[key].Name)] = i;
            surnames[surnamesValue.indexOf(this._women[key].Surname)] = i;
            pastas[pastasValue.indexOf(this._women[key].Pasta)] = i;
            wines[winesValue.indexOf(this._women[key].Wine)] = i;
            ages[agesValue.indexOf(this._women[key].Age)] = i;
            i++;
        }

        let shirtsComment = shirts.filter(v => v == "_").length == 5 ? "%" : "" ;
        let namesComment = names.filter(v => v == "_").length == 5 ? "%" : "";
        let surnamesComment = surnames.filter(v => v == "_").length == 5 ? "%" : "";
        let pastasComment = pastas.filter(v => v == "_").length == 5 ? "%" : "";
        let winesComment = wines.filter(v => v == "_").length == 5 ? "%" : "";
        let agesComment = ages.filter(v => v == "_").length == 5 ? "%" : "";

        let data = `
            ${shirtsComment}shirts = [${shirts}];
            ${namesComment}names = [${names}];
            ${surnamesComment}surnames = [${surnames}];
            ${pastasComment}pastas = [${pastas}];
            ${winesComment}wines = [${wines}];
            ${agesComment}ages = [${ages}];
        `;

        console.log(`${this.getProblemPath()} : data :`);
        console.log(data);

        return data;
    }
}

module.exports = Model1