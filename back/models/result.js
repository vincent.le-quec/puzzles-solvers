class Result {
    constructor(satisfiable, result) {
        this._satisfiable = satisfiable;
        this._result = result;
    }

    toJson() {
        return {
            satisfiable: this._satisfiable,
            result: null
        };
    }
}

module.exports = Result