const AbstractModel = require('./abstract_model.js');

let monitorsValue = ["monitor_13", "monitor_15", "monitor_15_6", "monitor_21_5", "monitor_27"];

class Model3 extends AbstractModel {
    constructor({ Monitor }) {
        super();
        this._monitor = Monitor;
    }

    getProblemPath() {
        return "pb_3";
    }

    toString() {
        return `Model3[n_queen='${this._n_queen}']`;
    }

    toDzn() {
        let monitors = ["_", "_", "_", "_", "_"];
        let processors = ["_", "_", "_", "_", "_"];
        let hard_disks = ["_", "_", "_", "_", "_"];
        let prices = ["_", "_", "_", "_", "_"];

        let i = 0;
        for (let monitor in this._monitor) {
            let processorsName = this._monitor[monitor].Processor;
            let hard_disksName = this._monitor[monitor]["Hard Disk"];
            let pricesName = this._monitor[monitor].Price;

            let processorKey = Object.keys(processorsName).indexOf(Object.keys(processorsName).filter(key => processorsName[key])[0]);
            let hard_diskKey = Object.keys(hard_disksName).indexOf(Object.keys(hard_disksName).filter(key => hard_disksName[key])[0]);
            let priceKey = Object.keys(pricesName).indexOf(Object.keys(pricesName).filter(key => pricesName[key])[0]);

            if (Object.keys(processorsName).filter(key => processorsName[key]).length > 1 ||
                Object.keys(hard_disksName).filter(key => hard_disksName[key]).length > 1 ||
                Object.keys(pricesName).filter(key => pricesName[key]).length > 1) {
                throw "Incorrect model !";
            }

            if ((processorKey != -1 && processors[processorKey] != "_") ||
                (hard_diskKey != -1 && hard_disks[hard_diskKey] != "_") ||
                (priceKey != -1 && prices[priceKey] != "_")) {
                throw "Incorrect model !";
            }

            if (processorKey != -1) processors[processorKey] = monitorsValue[i];
            if (hard_diskKey != -1) hard_disks[hard_diskKey] = monitorsValue[i];
            if (priceKey != -1) prices[priceKey] = monitorsValue[i];
            i++;
        }

        let processorsComment = processors.filter(v => v == "_").length == 5 ? "%" : "";
        let hard_disksComment = hard_disks.filter(v => v == "_").length == 5 ? "%" : "";
        let pricesComment = prices.filter(v => v == "_").length == 5 ? "%" : "";

        let data = `
            ${processorsComment}processors = [${processors}];
            ${hard_disksComment}hard_disks = [${hard_disks}];
            ${pricesComment}prices = [${prices}];
        `;

        console.log(`${this.getProblemPath()} : data :`);
        console.log(data);

        return data;
    }
}

module.exports = Model3