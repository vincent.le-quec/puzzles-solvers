const fs = require('fs');
const { execSync } = require("child_process");

const Result = require('./result.js');
const problemsFolder = "../minizinc";
const commandPath = `${problemsFolder}/exec_minizinc.sh`;
const separator = "----------";

class AbstractModel {

    constructor() {
        if (this.constructor === AbstractModel) {
            throw new TypeError('Abstract class "AbstractConfig" cannot be instantiated directly');
        }
    }

    toJson() {
        let json = {};
        for (let key in this) {
            json[key] = this[key]
        }
        return json;
    }

    toString() {
        throw new TypeError('this method needs to be overridden');
    }

    toDzn() {
        throw new TypeError('this method needs to be overridden');
    }

    getProblemsFolder() {
        return problemsFolder;
    }

    getProblemPath() {
        throw new TypeError('this method needs to be overridden');
    }

    getFileName() {
        let crypto = require('crypto');
        let shasum = crypto.createHash('sha1');
        shasum.update(`${this.toString()}`);
        let name = shasum.digest('hex') + ".dzn";
        // console.log(name);
        return name;
    }

    toContract(response) {
        return response.split(separator)[1];
    }

    solve() {

        let completeProblemPath = problemsFolder + "/" + this.getProblemPath() + "/";
        let completeDataPath = completeProblemPath + this.getFileName();
        // console.log(`Creating folder : ${completeDataPath}`);

        // create data file
        try {
            fs.writeFileSync(completeDataPath, this.toDzn());
        } catch(error) {
            return new Result(false, null);
        }

        // execute minizinc
        let cmd = `bash ${commandPath} ${completeProblemPath}model.mzn ${completeDataPath} | cat`;
        // console.log(`Command='${cmd}'`);
        let result = execSync(cmd);

        fs.unlinkSync(completeDataPath);

        // return solver response
        // console.log(result);
        console.log(`result = ${result}`);

        if (`${result}`.includes("=====UNSATISFIABLE=====")) {
            return new Result(false, null);
        }

        return new Result(true, this.toContract(result.toString()));
    }
}

module.exports = AbstractModel;