const AbstractModel = require('./abstract_model.js');

let namesValue = ["Jessica", "Laurie", "Mark", "Mary", "Sally"];

class Model2 extends AbstractModel {
    constructor({ Name, Time, Day }) {
        super();
        this._name = Name;
        this._time = Time;
        this._day = Day;
    }

    getProblemPath() {
        return "pb_2";
    }

    toString() {
        return `Model2[n_queen='${this._n_queen}']`;
    }

    toDzn() {
        let films = ["_", "_" ,"_", "_", "_"];
        let days = ["_", "_" ,"_", "_", "_"];
        let times = ["_", "_" ,"_", "_", "_"];

        let i = 0;
        for(let name in this._name) {
            let filmsName = this._name[name].Film;
            let daysName = this._name[name].Day;
            let timesName = this._name[name].Time;

            let filmKey = Object.keys(filmsName).indexOf(Object.keys(filmsName).filter(key => filmsName[key])[0]);
            let dayKey = Object.keys(daysName).indexOf(Object.keys(daysName).filter(key => daysName[key])[0]);
            let timeKey = Object.keys(timesName).indexOf(Object.keys(timesName).filter(key => timesName[key])[0]);

            // verify we do not have multiple values by line
            if(Object.keys(filmsName).filter(key => filmsName[key]).length > 1 ||
                Object.keys(daysName).filter(key => daysName[key]).length > 1 ||
                Object.keys(timesName).filter(key => timesName[key]).length > 1) {
                throw "Incorrect model !";
            }

            // verify we do not have multiple values by columns
            if((films[filmKey] != "_" && filmKey != -1) ||
                (days[dayKey] != "_" && dayKey != -1) ||
                (times[timeKey] != "_" && timeKey != -1)) {
                throw "Incorrect model !";
            }

            if(filmKey != -1)films[filmKey] = namesValue[i];
            if(dayKey != -1)days[dayKey] = namesValue[i];
            if(timeKey != -1)times[timeKey] = namesValue[i];
            i++;
        }

        let filmsComment = films.filter(v => v == "_").length == 5 ? "%" : "" ;
        let daysComment = days.filter(v => v == "_").length == 5 ? "%" : "" ;
        let timesComment = times.filter(v => v == "_").length == 5 ? "%" : "" ;

        let data = `
            ${filmsComment}films = [${films}];
            ${daysComment}days = [${days}];
            ${timesComment}times = [${times}];
        `;

        console.log(`${this.getProblemPath()} : data :`);
        console.log(data);

        return data;
    }
}

module.exports = Model2