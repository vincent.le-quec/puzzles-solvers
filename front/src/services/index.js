import ApiUtils from './utils';

// const API_URL = "//localhost:3000";
const API_URL = "";

export default {
    async pb1(data){
        return await fetch(`${ API_URL }/api/pb1`, {
            method: 'POST',
            headers: new Headers({
                'Content-Type' : 'application/json',
            }),
            body: JSON.stringify(data)
        }).then(ApiUtils.extract);
    },
    async pb2(data){
        return await fetch(`${ API_URL }/api/pb2`, {
            method: 'POST',
            headers: new Headers({
                'Content-Type' : 'application/json',
            }),
            body: JSON.stringify(data)
        }).then(ApiUtils.extract);
    },
    async pb3(data){
        return await fetch(`${ API_URL }/api/pb3`, {
            method: 'POST',
            headers: new Headers({
                'Content-Type' : 'application/json',
            }),
            body: JSON.stringify(data)
        }).then(ApiUtils.extract);
    },
}