import Vue from 'vue'
import VueRouter from 'vue-router'
import Pasta from '../views/Pasta.view.vue'
import Movie from '../views/Movie.view.vue'
import Computer from '../views/Computer.view.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Pasta',
    component: Pasta
  },
  {
    path: '/movie',
    name: 'Movie',
    component: Movie
  },
  {
    path: '/computer',
    name: 'Computer',
    component: Computer
  },
]

const router = new VueRouter({
  routes
})

export default router
