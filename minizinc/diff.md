# Problem 1

## Correct answer
|   Data   | Woman #1  | Woman #2 | Woman #3 |  Woman #4  | Woman #5  |
| :------: | :-------: | :------: | :------: | :--------: | :-------: |
|  shirts  |  yellow   |   blue   |   red    |   white    |   green   |
|  names   | Victoria  |  Leslie  |  Andrea  |   Julie    |   Holly   |
| surnames |   Davis   |  Miller  |  Brown   |   Wilson   |   Lopes   |
|  pastas  |  Ravioli  | Farfalle |  Penne   |  Lasagne   | Spaghetti |
|  wines   | Argentine | Chilean  | Italian  | Australian |  French   |
|   ages   | years_50  | years_35 | years_30 |  years_40  | years_45  |

# Problem 2

## Correct answer
| Data  |    Jessica    |   Laurie    |   Mark    |    Mary    |  Sally   |
| :---: | :-----------: | :---------: | :-------: | :--------: | :------: |
| films | Donnie_Brasco | The_Recruit | Scarecrow | Minutes_88 | Scarface |
| days  |   Wednesday   |   Friday    |  Tuesday  |   Monday   | Thursday |
| time  |    pm_8_30    |   pm_7_35   |  pm_8_45  |  pm_8_20   | pm_7_40  |

## Other answers

### First
| Data  | Jessica |  Laurie  | Mark  | Mary  |    Sally    |
| :---: | :-----: | :------: | :---: | :---: | :---------: |
| films |         | Scarface |       |       | The_Recruit |
| days  |         | Thursday |       |       |   Friday    |
| time  |         | pm_7_40  |       |       |   pm_7_35   |

### Second
| Data  | Jessica |  Laurie  | Mark  |    Mary     |   Sally    |
| :---: | :-----: | :------: | :---: | :---------: | :--------: |
| films |         | Scarface |       | The_Recruit | Minutes_88 |
| days  |         | Thursday |       |   Friday    |   Monday   |
| time  |         | pm_7_40  |       |   pm_7_35   |  pm_8_20   |


# Problem 3

## Correct answer
|    Data    | monitor_13 | monitor_15 | monitor_15_6 | monitor_21_5 | monitor_27  |
| :--------: | :--------: | :--------: | :----------: | :----------: | :---------: |
| processors |  MHz_2_0   |  MHz_2_3   |   MHz_2_5    |   MHz_2_7    |   MHz_3_1   |
| hard_disks |   Gb_320   |   Gb_250   |    Gb_500    |   Gb_1024    |   Gb_750    |
|   prices   | price_999  | price_699  | price_1_349  | price_1_149  | price_1_649 |