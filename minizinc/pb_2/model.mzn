include "alldifferent.mzn";

enum namesValue = {Jessica, Laurie, Mark, Mary, Sally};
enum filmsValue = {Minutes_88, Donnie_Brasco, Scarecrow, Scarface, The_Recruit};
enum daysValue = {Monday, Tuesday, Wednesday, Thursday, Friday};
enum timesValue = {pm_7_35, pm_7_40, pm_8_20, pm_8_30, pm_8_45};

array[filmsValue] of var namesValue: films;
array[daysValue] of var namesValue: days;
array[timesValue] of var namesValue: times;

constraint alldifferent(films);
constraint alldifferent(days);
constraint alldifferent(times);

function var int: dayToNumber(var daysValue: d) =
    if d == Monday then 1
    elseif d == Tuesday then 2
    elseif d == Wednesday then 3
    elseif d == Thursday then 4
    else 5
    endif;

function var int: timeToNumber(var timesValue: t) =
    if t == pm_7_35 then 455
    elseif t == pm_7_40 then 460
    elseif t == pm_8_20 then 500
    elseif t == pm_8_30 then 510
    else 525
    endif;

predicate isMan(var namesValue: v) =
    if v == Mark \/ v == Laurie then true
    else false
    endif;

predicate nextTo(var int: a, var int: b) =
    a == b + 1 \/ a == b - 1;

predicate is20hundreds(var filmsValue: f) =
    f == The_Recruit \/ f == Minutes_88;

predicate consecutiveDays(var daysValue: d1, var daysValue: d2) =
    nextTo(dayToNumber(d1), dayToNumber(d2));

% 1. Of the 20-hundreds releases, neither of which was Jessica's choice
var filmsValue: q1_film_1;
constraint Jessica == films[q1_film_1] /\ not is20hundreds(q1_film_1);

%, one opened the week and one closed the week.
var filmsValue: q1_film_2;
var filmsValue: q1_film_3;
constraint days[Monday] == films[q1_film_2] /\ is20hundreds(q1_film_2);
constraint days[Friday] == films[q1_film_3] /\ is20hundreds(q1_film_3);

% 2. The latest of the 19-hundreds releases was shown at 30 minutes past the hour.
var timesValue: q2_time;
constraint films[Donnie_Brasco] == times[q2_time] /\ timeToNumber(q2_time) mod 60 == 30;

% 3. The releases shown before 8:00 pm were on consecutive days
var daysValue: q3_day1;
var daysValue: q3_day2;
constraint times[pm_7_35] == days[q3_day1];
constraint times[pm_7_40] == days[q3_day2];
constraint consecutiveDays(q3_day1, q3_day2);
%, as were the releases shown after 8:00 pm.
constraint q3_day1 == Monday \/ q3_day2 == Monday \/ q3_day1 == Friday \/ q3_day2 == Friday;

% 4. One of the men and one of the women had a showing before 8:00 pm
var namesValue: man;
var namesValue: woman;
constraint isMan(man);
constraint not isMan(woman);
constraint (man == times[pm_7_35] /\ woman == times[pm_7_40])
        \/ (man == times[pm_7_40] /\ woman == times[pm_7_35]);
%, but neither was mid-week.
constraint man != days[Wednesday] /\ woman != days[Wednesday];

% 5. Mark, whose choice was Scarecrow
constraint Mark == films[Scarecrow];
%, had a showing at a time of one hour and five minutes after that of Scarface.
var timesValue: q5_time_mark;
var timesValue: q5_time_scarface;
constraint Mark == times[q5_time_mark];
constraint films[Scarface] == times[q5_time_scarface];
constraint timeToNumber(q5_time_mark) == timeToNumber(q5_time_scarface) + 65;

% 6. Neither Miss Farmer nor Miss Peters had a showing on an even-numbered day
predicate showOnEvenDay(var daysValue: d) =
    dayToNumber(d) mod 2 == 0;
var daysValue: q6_jessica;
constraint Jessica == days[q6_jessica] /\ not showOnEvenDay(q6_jessica);
var daysValue: q6_mary;
constraint Mary == days[q6_mary] /\ not showOnEvenDay(q6_mary);

% 7. 88 Minutes showed at a time both 40 minutes to the hour
var timesValue: q7_time_88;
constraint films[Minutes_88] == times[q7_time_88];
constraint timeToNumber(q7_time_88) mod 60 == 20;
% and 40 minutes after the Thursday showing.
var timesValue: q7_time_thursday;
constraint times[q7_time_thursday] == days[Thursday];
constraint timeToNumber(q7_time_88) == timeToNumber(q7_time_thursday) + 40;

%search
solve satisfy;

% solve satisfy
output [ "films" ++ show(films) ++
        "\ndays" ++ show(days) ++
        "\ntimes" ++ show(times)
]