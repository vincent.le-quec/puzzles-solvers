function readme() {
    echo "Command example :"
    echo "./cmd resolv.mzn data.dzn"
}

function verifyFile() {
    if ! test -f "$1"; then
        echo "$1 does not exist !"
        exit 1
    fi
}

# exit if nb of argument >= 4
if [ "$#" -ge "4" ]; then
    echo "Too much arguments !"
    readme
    exit 2
fi

# initialise our variables
all=""
model="$1"
data=""

# verify if model file exists
verifyFile "$1"
model="$1"

# for one agument
if [ "$#" -eq "2" ]; then

    # check for all solutions
    if [ "$2" = "--all" ] || [ "$2" = "-a" ];then
        $all="-a"

    # check data file
    else
        verifyFile "$2"
        data=$2
    fi

# for two arguments
elif [ "$#" -eq "3" ]; then

    # all first and data second
    if [ "$2" = "--all" ] || [ "$2" = "-a" ];then
        all="-a"
        verifyFile "$3"
        data="$3"

    # all second and data first
    elif [ "$3" = "--all" ] || [ "$3" = "-a" ];then
        all="-a"
        verifyFile "$2"
        data="$2"

    # exit when unknown
    else
        echo "Unknown tags $2 and $3 !";
        readme
        exit 3
    fi
fi

echo "minizinc $all --solver Gecode $model $data"
echo "----------"
minizinc $all --solver Gecode $model $data

