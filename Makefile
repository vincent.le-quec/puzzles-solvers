SHELL:=/bin/bash

all: install build serve

install:
	make -C back install
	npm i --prefix front

build:
	npm run build --prefix front

dev:
	make -C back dev

serve:
	make -C back prod

.PHONY: install build dev serve