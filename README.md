# Puzzles Solvers

## Requirements
Nodejs >= v12
MiniZinc >= 2.5.3

## Execution
Do everything needed :
```shell
make
```

Install project dependencies :
```shell
make install
```

Build front application :
```shell
make build
```

Run server in development mode :
```shell
make dev
```

Run server in production mode :
```shell
make serve
```